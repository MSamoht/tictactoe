export interface Game {
    board: number[][]
    turn: number
}

export function createGame(): Game {
    return {
        board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
        turn: 0
    }
}

export function cloneGame(game: Game): Game {
    let board = []
    game.board.forEach(row => {
        let r = []
        row.forEach(col => {
            r.push(col)
        })
        board.push(r)
    })
    return {
        board,
        turn: game.turn
    }
}

export function canPlay(game: Game, x: number, y: number): boolean {
    return game.board[y][x] === 0
}

export function play(game: Game, x: number, y: number): Game {
    if (canPlay(game, x, y)) {
        let g = cloneGame(game)
        g.board[y][x] = getToken(getPlayerTurn(g))
        g.turn++
        return g
    }
    return game
}

export function getPlayerTurn(game: Game): number {
    return game.turn % 2 === 0 ? 1 : 2
}

export function getToken(player: number): number {
    return player === 1 ? 1 : 4
}

export function getWinner(game: Game): number {
    let r1 = game.board[0][0] + game.board[0][1] + game.board[0][2]
    let r2 = game.board[1][0] + game.board[1][1] + game.board[1][2]
    let r3 = game.board[2][0] + game.board[2][1] + game.board[2][2]

    let c1 = game.board[0][0] + game.board[1][0] + game.board[2][0]
    let c2 = game.board[0][1] + game.board[1][1] + game.board[2][1]
    let c3 = game.board[0][2] + game.board[1][2] + game.board[2][2]

    let d1 = game.board[0][0] + game.board[1][1] + game.board[2][2]
    let d2 = game.board[0][2] + game.board[1][1] + game.board[2][0]

    if (
        r1 === 3 ||
        r2 === 3 ||
        r3 === 3 ||
        c1 === 3 ||
        c2 === 3 ||
        c3 === 3 ||
        d1 === 3 ||
        d2 === 3
    ) {
        return 1
    } else if (
        r1 === 12 ||
        r2 === 12 ||
        r3 === 12 ||
        c1 === 12 ||
        c2 === 12 ||
        c3 === 12 ||
        d1 === 12 ||
        d2 === 12
    ) {
        return 2
    } else {
        return 0
    }
}
