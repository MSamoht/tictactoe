export interface Game {
    board: number[][];
    turn: number;
}
export declare function createGame(): Game;
export declare function cloneGame(game: Game): Game;
export declare function canPlay(game: Game, x: number, y: number): boolean;
export declare function play(game: Game, x: number, y: number): Game;
export declare function getPlayerTurn(game: Game): number;
export declare function getToken(player: number): number;
export declare function getWinner(game: Game): number;
