"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function createGame() {
    return {
        board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
        turn: 0
    };
}
exports.createGame = createGame;
function cloneGame(game) {
    var board = [];
    game.board.forEach(function (row) {
        var r = [];
        row.forEach(function (col) {
            r.push(col);
        });
        board.push(r);
    });
    return {
        board: board,
        turn: game.turn
    };
}
exports.cloneGame = cloneGame;
function canPlay(game, x, y) {
    return game.board[y][x] === 0;
}
exports.canPlay = canPlay;
function play(game, x, y) {
    if (canPlay(game, x, y)) {
        var g = cloneGame(game);
        g.board[y][x] = getToken(getPlayerTurn(g));
        g.turn++;
        return g;
    }
    return game;
}
exports.play = play;
function getPlayerTurn(game) {
    return game.turn % 2 === 0 ? 1 : 2;
}
exports.getPlayerTurn = getPlayerTurn;
function getToken(player) {
    return player === 1 ? 1 : 4;
}
exports.getToken = getToken;
function getWinner(game) {
    var r1 = game.board[0][0] + game.board[0][1] + game.board[0][2];
    var r2 = game.board[1][0] + game.board[1][1] + game.board[1][2];
    var r3 = game.board[2][0] + game.board[2][1] + game.board[2][2];
    var c1 = game.board[0][0] + game.board[1][0] + game.board[2][0];
    var c2 = game.board[0][1] + game.board[1][1] + game.board[2][1];
    var c3 = game.board[0][2] + game.board[1][2] + game.board[2][2];
    var d1 = game.board[0][0] + game.board[1][1] + game.board[2][2];
    var d2 = game.board[0][2] + game.board[1][1] + game.board[2][0];
    if (r1 === 3 || r2 === 3 || r3 === 3 || c1 === 3 || c2 === 3 || c3 === 3 || d1 === 3 || d2 === 3) {
        return 1;
    }
    else if (r1 === 12 || r2 === 12 || r3 === 12 || c1 === 12 || c2 === 12 || c3 === 12 || d1 === 12 || d2 === 12) {
        return 2;
    }
    else {
        return 0;
    }
}
exports.getWinner = getWinner;
//# sourceMappingURL=game.js.map