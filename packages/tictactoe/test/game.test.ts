import { expect } from 'chai'
import {
    createGame,
    cloneGame,
    canPlay,
    getPlayerTurn,
    getToken,
    play,
    getWinner
} from '../src/game'

describe('game', () => {
    describe('createGame', () => {
        it('should create a game', () => {
            let game = createGame()
        })
    })

    describe('cloneGame', () => {
        it('should clone the game', () => {
            let game = createGame()
            let game2 = cloneGame(game)
            game.board[0][0] = 1
            game.turn = 2
            expect(game.board[0][0]).to.be.eq(1)
            expect(game2.board[0][0]).to.be.eq(0)
            expect(game.turn).to.be.eq(2)
            expect(game2.turn).to.be.eq(0)
        })
    })

    describe('canPlay', () => {
        it('should return if player can play at coord', () => {
            let game = createGame()
            game.board[0][0] = 1
            expect(canPlay(game, 0, 0)).to.be.false
            expect(canPlay(game, 1, 1)).to.be.true
        })
    })

    describe('getPlayerTurn', () => {
        it('should return the current player turn', () => {
            let game = createGame()
            expect(getPlayerTurn(game)).to.be.eq(1)
            game.turn += 1
            expect(getPlayerTurn(game)).to.be.eq(2)
            game.turn += 1
            expect(getPlayerTurn(game)).to.be.eq(1)
            game.turn += 1
            expect(getPlayerTurn(game)).to.be.eq(2)
        })
    })
    describe('getToken', () => {
        it('should return the current player token', () => {
            let game = createGame()
            expect(getToken(getPlayerTurn(game))).to.be.eq(1)
            game.turn += 1
            expect(getToken(getPlayerTurn(game))).to.be.eq(4)
            game.turn += 1
            expect(getToken(getPlayerTurn(game))).to.be.eq(1)
            game.turn += 1
            expect(getToken(getPlayerTurn(game))).to.be.eq(4)
        })
    })

    describe('play', () => {
        it('sould return a new state', () => {
            let game = createGame()
            game = play(game, 0, 1)
            expect(game.turn).to.be.eq(1)
            expect(game.board[1][0]).to.be.eq(1)
        })
        it('sould return the old state', () => {
            let game = createGame()
            game = play(game, 0, 1)
            let ngame = play(game, 0, 1)
            expect(game).to.be.eq(ngame)
        })
    })

    describe('getWinner', () => {
        it('should return 1', () => {
            let game = createGame()
            game.board[0][0] = 1
            game.board[0][1] = 1
            game.board[0][2] = 1
            expect(getWinner(game)).to.be.eq(1)
        })
        it('should return 2', () => {
            let game = createGame()
            game.board[0][0] = 4
            game.board[0][1] = 4
            game.board[0][2] = 4
            expect(getWinner(game)).to.be.eq(2)
        })
        it('should return 0', () => {
            let game = createGame()
            game.board[0][0] = 4
            game.board[0][1] = 1
            game.board[0][2] = 4
            expect(getWinner(game)).to.be.eq(0)
        })
    })
})
