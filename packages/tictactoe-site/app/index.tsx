import * as React from 'react'
import { render } from 'react-dom'

import Board from './component/Board'

render(<Board />, document.querySelector('#app'))