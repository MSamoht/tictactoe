import * as React from 'react'

import Cell from './Cell'
import {
    createGame,
    Game,
    canPlay,
    cloneGame,
    play,
    getWinner
} from 'tictactoe'

export interface IProps {}
export interface IState {
    game: Game
    p1: number
    p2: number
    draw: 0
}

export default class Board extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props)
        this.state = {
            game: createGame(),
            p1: 0,
            p2: 0,
            draw: 0
        }
    }

    onClick(x, y) {
        let game = cloneGame(this.state.game)
        if (canPlay(game, x, y)) {
            let newGame = play(game, x, y)
            let winner = getWinner(newGame)
            let state = {} as any
            if (winner !== 0) {
                state.game = createGame()
                state['p' + winner] = this.state['p' + winner] + 1
            } else if (newGame.turn === 9) {
                state.game = createGame()
                state.draw = this.state.draw + 1
            } else {
                state.game = newGame
            }
            this.setState(state)
        }
    }

    render(): JSX.Element {
        return (
            <div>
                <p>player 1 : {this.state.p1}</p>
                <p>player 2 : {this.state.p2}</p>
                <p>draw : {this.state.draw}</p>
                <p>turn : {this.state.game.turn}</p>
                <div style={{ width: '600px', height: '600px' }}>
                    {this.state.game.board.map((r, y) => {
                        return r.map((c, x) => {
                            return (
                                <Cell
                                    value={c}
                                    key={`${x}-${y}`}
                                    click={e => this.onClick(x, y)}
                                />
                            )
                        })
                    })}
                </div>
            </div>
        )
    }
}
