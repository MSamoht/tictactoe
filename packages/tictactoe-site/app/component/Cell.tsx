import * as React from 'react'

export interface IProps {
    value: number
    click: (...any) => void
}
export interface IState {}

export default class Cell extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props)
        this.state = {}
    }

    get value() {
        if (this.props.value === 0) return '-'
        return this.props.value === 1 ? 'O' : 'X'
    }

    get style(): React.CSSProperties {
        return {
            width: '200px',
            height: '200px',
            border: 'solid black 1px',
            display: 'inline-block',
            lineHeight: '200px',
            textAlign: 'center',
            fontSize: '56px',
            cursor: 'pointer'
        }
    }

    render(): JSX.Element {
        return (
            <div style={this.style} onClick={this.props.click}>
                {this.value}
            </div>
        )
    }
}
